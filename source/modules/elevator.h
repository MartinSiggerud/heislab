#pragma once
#include <time.h>
#include "../driver/elevio.h"

struct Elevator{
    int current_floor;
    int next_floor;
    int last_floor;
    int door_state;
    int stop;
    MotorDirection direction;
    MotorDirection last_direction;
    time_t time;
};
typedef struct Elevator Elevator;

void elevator_current_floor(Elevator *p_elevator);

void elevator_last_floor(Elevator *p_elevator);

