#include "movement.h"

void movement_open_door(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons){
    if(p_elevator->time == -1){
        timer_reset(p_elevator);
        p_elevator->door_state = 1;
        order_complete_remove_button(p_elevator,p_hall_buttons,p_cab_buttons);
    }
    else if(timer_left(p_elevator)){
        p_elevator->door_state = 1;
        order_complete_remove_button(p_elevator,p_hall_buttons,p_cab_buttons);
    }
    return;
}

void movement_close_door(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons){
    if(!timer_left(p_elevator)){
        order_complete_remove_button(p_elevator,p_hall_buttons,p_cab_buttons);
        p_elevator->door_state = 0;
    }
    return;
}

void movement_obstruction(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons){
    if (!elevio_obstruction() && p_elevator->door_state == 1){
        timer_reset(p_elevator);
        p_elevator->door_state = 1;
        order_complete_remove_button(p_elevator,p_hall_buttons,p_cab_buttons);
    }
    return;
}

void movement_stop(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons){
    if (p_elevator->current_floor != -1 && elevio_stopButton()){
        p_elevator->stop = 1;
        p_elevator->time = -1;
        movement_open_door(p_elevator, p_hall_buttons ,p_cab_buttons);
        order_remove_all_buttons(p_hall_buttons,p_cab_buttons);
    }
    else if (p_elevator->current_floor == -1 && elevio_stopButton()){
        p_elevator->stop = 1;
        p_elevator->next_floor = p_elevator->current_floor;
        order_remove_all_buttons(p_hall_buttons,p_cab_buttons);
    }
    else{
        p_elevator->stop = 0;
    }
    return;   
}

void movement_elevator(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons){
    int delta = 0;
    if(p_elevator->stop){
        elevio_motorDirection(DIRN_STOP);
        p_elevator->direction = DIRN_STOP;
    }
    else if(p_elevator->current_floor != p_elevator->next_floor && p_elevator->door_state == 0){
        p_elevator->time = -1;
        delta = p_elevator->next_floor - p_elevator->last_floor;
        if(delta < 0){
            elevio_motorDirection(DIRN_DOWN);
            p_elevator->direction = DIRN_DOWN;
            p_elevator->last_direction = DIRN_DOWN;
        }
        else if(delta > 0){
            elevio_motorDirection(DIRN_UP);
            p_elevator->direction = DIRN_UP;
            p_elevator->last_direction = DIRN_UP;
        }
        else{
            if(p_elevator->last_direction == DIRN_DOWN){
                elevio_motorDirection(DIRN_UP);
                p_elevator->direction = DIRN_UP;
            }
            else if(p_elevator->last_direction == DIRN_UP){
                elevio_motorDirection(DIRN_DOWN);
                p_elevator->direction = DIRN_DOWN;
            }
        }
    }
    else if(p_elevator->current_floor == p_elevator->next_floor && p_elevator->current_floor != -1){
        elevio_motorDirection(DIRN_STOP);
        p_elevator->direction = DIRN_STOP;
        p_elevator->last_floor = p_elevator->current_floor;
        movement_open_door(p_elevator,p_hall_buttons,p_cab_buttons);
        movement_obstruction(p_elevator,p_hall_buttons,p_cab_buttons);
        movement_close_door(p_elevator, p_hall_buttons, p_cab_buttons);

    }
    else{
        movement_obstruction(p_elevator,p_hall_buttons,p_cab_buttons);
        movement_close_door(p_elevator, p_hall_buttons, p_cab_buttons);
    }
    return; 
}