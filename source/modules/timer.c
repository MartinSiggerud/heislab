#include "timer.h"

void timer_reset(Elevator *p_elevator){
    p_elevator->time = time(NULL) + 3;
    return;
}

int timer_left(Elevator *p_elevator){
    if (p_elevator->time != -1)return (time(NULL) < p_elevator->time);
    else return 0;
}



