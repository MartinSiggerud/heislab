/**
 * @file startup.h
 * @brief Takes care of startup procedure
 * @version 0.1
 * @date 2022-02-23
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once
#include "../driver/elevio.h"
#include "elevator.h"
#include "order.h"

/**
 * @brief Checks if p_elevator is at a floor, else it moves upwards untill it finds a floor 
 * 
 * @return returns a int where 1 means success
 */
int startup_initialize(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons);
