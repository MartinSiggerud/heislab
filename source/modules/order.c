#include "order.h"



void order_set_hall_buttons(Hall_buttons *p_hall_buttons, Elevator *p_elevator){
    int b_hall_up;
    int b_hall_down;
    for (int i = 0; i < N_BUTTONS; i++){
        b_hall_up = elevio_callButton(i,BUTTON_HALL_UP);
        b_hall_down = elevio_callButton(i+1,BUTTON_HALL_DOWN);
        if(p_elevator->current_floor == i && b_hall_up){
            timer_reset(p_elevator);
        }
        else if(b_hall_up){
            p_hall_buttons->hall_up_buttons[i] = b_hall_up; 
        }
        if(p_elevator->current_floor == i+1 && b_hall_down){
            timer_reset(p_elevator);
        }
        else if(b_hall_down){
            p_hall_buttons->hall_down_buttons[i] = b_hall_down; 
        }
    }
    return;
}

void order_set_cab_buttons(Cab_buttons *p_cab_buttons, Elevator *p_elevator){
    int b_cabin;
    for (int i = 0; i < N_BUTTONS+1; i++){
        b_cabin = elevio_callButton(i, BUTTON_CAB);
        if(p_elevator->current_floor == i && b_cabin){
            timer_reset(p_elevator);
        }
        else if(b_cabin){
            p_cab_buttons->button[i] = b_cabin;
        }
    }
    return;
}

void order_complete_remove_button(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons){
    if(p_elevator->current_floor == p_elevator->next_floor){
        for (int i = 0; i < N_BUTTONS; i++){
            if(p_hall_buttons->hall_down_buttons[i] == 1 && i+1 == p_elevator->current_floor && p_elevator->door_state == 1){
                p_hall_buttons->hall_down_buttons[i] = 0; 
            }
            if(p_hall_buttons->hall_up_buttons[i] == 1 && i == p_elevator->current_floor && p_elevator->door_state == 1){
                p_hall_buttons->hall_up_buttons[i] = 0;
            }       
        }
        for (int i = 0; i < N_FLOORS; i++){
            if(p_cab_buttons->button[i] == 1 && i == p_elevator->current_floor && p_elevator->door_state == 1){
                p_cab_buttons->button[i] = 0; 
            }
        }
    }
    return;
}
void order_remove_all_buttons(Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons){
    for (int i = 0; i < N_BUTTONS; i++){
        p_hall_buttons->hall_down_buttons[i] = 0;
        p_hall_buttons->hall_up_buttons[i] = 0;
    }
    for (int i = 0; i < N_FLOORS; i++){
        p_cab_buttons->button[i] = 0;
    }
    return;
}

int order_check_if_empty(Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons){
    int b_hall_order = 0;
    for (int i = 0; i < N_BUTTONS; i++){
        if(p_hall_buttons->hall_down_buttons[i]){
            b_hall_order = 1;
            break;
        }
        if(p_hall_buttons->hall_up_buttons[i]){
            b_hall_order = 1;
            break;
        }
    }
    int b_cab_order = 0;
    for (int i = 0; i < N_FLOORS; i++){
        if(p_cab_buttons->button[i]){
            b_cab_order = 1;
            break;
        }
    }
    return !(b_hall_order||b_cab_order);
}



void order_find_next_floor(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons){
    if(order_check_if_empty(p_hall_buttons, p_cab_buttons)){
        p_elevator->next_floor = p_elevator->current_floor;
        return;
    }

    int top_refrence_floor = -1;
    int bottom_refrence_floor = -1;

    //for-loop for finding top- and bottom refrence floor
    for (int i = 0; i < N_FLOORS; i++){
        if(i < N_BUTTONS){
            if(bottom_refrence_floor == -1 && p_hall_buttons->hall_up_buttons[i] == 1){
                bottom_refrence_floor = i;
            }
            else if(i > 0 && bottom_refrence_floor == -1 && p_hall_buttons->hall_down_buttons[i-1] == 1){
                bottom_refrence_floor = i;
            }
        }
        if(i > 0){
            if(p_hall_buttons->hall_down_buttons[i-1]){
                top_refrence_floor = i;
            }
            else if(i < N_BUTTONS && p_hall_buttons->hall_up_buttons[i]){
                top_refrence_floor = i;
            }
        }
        if(bottom_refrence_floor == -1 && p_cab_buttons->button[i] == 1 ){
            bottom_refrence_floor = i;
        }
        if(p_cab_buttons->button[i]){
            top_refrence_floor = i;
        } 
    }
    //if statements for choosing optimal next_floor refrence 
    if(p_elevator->last_direction == DIRN_DOWN){
        if(bottom_refrence_floor != -1){
            p_elevator->next_floor = bottom_refrence_floor;
        }
        else{
            p_elevator->next_floor = top_refrence_floor;
        }
    }
    else if(p_elevator->last_direction == DIRN_UP){
        if(top_refrence_floor != -1){
            p_elevator->next_floor = top_refrence_floor;
        }
        else{
            p_elevator->next_floor = bottom_refrence_floor;
        }
    }
    return;
}

void order_check_next_floor(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons){
    if (p_elevator->current_floor != -1){
        if(p_cab_buttons->button[p_elevator->current_floor]){
            p_elevator->next_floor = p_elevator->current_floor;
        }
        else if(p_elevator->direction == DIRN_DOWN){
            if(p_elevator->current_floor > 0 && p_hall_buttons->hall_down_buttons[p_elevator->current_floor-1]){
                p_elevator->next_floor = p_elevator->current_floor;
            }
        }
        else if(p_elevator->direction == DIRN_UP){
            if(p_elevator->current_floor < 3 && p_hall_buttons->hall_up_buttons[p_elevator->current_floor]){
                p_elevator->next_floor = p_elevator->current_floor;
            }
        }
    }
    return;
}
