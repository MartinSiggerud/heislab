#include "elevator.h"

void elevator_current_floor(Elevator *p_elevator){
    p_elevator->current_floor = elevio_floorSensor();
    return;
} 

void elevator_last_floor(Elevator *p_elevator){
    if(p_elevator->current_floor != -1){
        p_elevator->last_floor = p_elevator->current_floor;
    }
    return;
}
