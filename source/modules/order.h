#pragma once
#include "../driver/elevio.h"
#include "elevator.h"
#include "timer.h"
#include <stdio.h>

struct Hall_buttons{
    int hall_up_buttons[N_BUTTONS];
    int hall_down_buttons[N_BUTTONS];
};
typedef struct Hall_buttons Hall_buttons;

struct Cab_buttons{
    int button[N_FLOORS];
};
typedef struct Cab_buttons Cab_buttons;

void order_set_hall_buttons(Hall_buttons *p_hall_buttons, Elevator *p_elevator);

void order_set_cab_buttons(Cab_buttons *p_cab_buttons,  Elevator *p_elevator);

void order_complete_remove_button(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons);

void order_remove_all_buttons(Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons);

int order_check_if_empty(Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons);

void order_find_next_floor(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons);

void order_check_next_floor(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons);


