/**
 * @file timer.h
 * @brief h file containing timer  
 * @version 0.2
 * @date 2022-02-23
 * 
 * @copyright Copyright (c) 2022
 */

#pragma once
#include "../driver/elevio.h"
#include "elevator.h"
#include <time.h>

void timer_reset(Elevator *p_elevator);

int timer_left(Elevator *p_elevator);

