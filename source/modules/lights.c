#include "lights.h"

void lights_floor(Elevator *p_elevator){
    if (p_elevator->last_floor !=  -1){
        elevio_floorIndicator(p_elevator->last_floor);
    }
    return; 
}  

void ligths_hall(Hall_buttons *p_hall_buttons){
    for (int i = 0; i < N_BUTTONS; i++){
        if (p_hall_buttons->hall_down_buttons[i] == 1){
            elevio_buttonLamp(i+1, BUTTON_HALL_DOWN,1);
        }
        else{
            elevio_buttonLamp(i+1, BUTTON_HALL_DOWN,0);
        }
        if(p_hall_buttons->hall_up_buttons[i] == 1){
            elevio_buttonLamp(i, BUTTON_HALL_UP,1);
        }
        else{
            elevio_buttonLamp(i, BUTTON_HALL_UP,0);
        }
    }
    return;
}

void lights_cab(Cab_buttons *p_cab_buttons){
    for (int i = 0; i < N_FLOORS; i++){
        if(p_cab_buttons->button[i] == 1){
            elevio_buttonLamp(i, BUTTON_CAB, 1);
        }
        else{
            elevio_buttonLamp(i, BUTTON_CAB, 0);
        }
    }
    return;
}

void lights_door(Elevator *p_elevator){
    if (p_elevator->door_state){
        elevio_doorOpenLamp(1);
    }
    else{
        elevio_doorOpenLamp(0);
    }
    return;
}

void lights_stop(Elevator *p_elevator){
    if(p_elevator->stop){
        elevio_stopLamp(1);
    }
    else{
        elevio_stopLamp(0);
    }
    return;
}
