#include "startup.h"

int startup_initialize(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons){
    elevio_init();

    int floor;
    do{
        floor = elevio_floorSensor();
        elevio_motorDirection(DIRN_DOWN);
    }while(floor == -1);
    elevio_motorDirection(DIRN_STOP);

    p_elevator->current_floor = floor;
    p_elevator->next_floor = floor;
    p_elevator->last_floor = floor;
    p_elevator->door_state = 0;
    p_elevator->direction = DIRN_STOP;
    p_elevator->last_direction = DIRN_DOWN;
    p_elevator->time = -1;
    p_elevator->stop = 0;

    order_remove_all_buttons(p_hall_buttons,p_cab_buttons);
    
    return 1;
}