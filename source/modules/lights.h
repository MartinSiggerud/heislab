/**
 * @file lights.h
 * @brief h file containing lights 
 * @version 0.2
 * @date 2022-02-23
 * 
 * @copyright Copyright (c) 2022
 */

#pragma once
#include "../driver/elevio.h"
#include "elevator.h"
#include "order.h"
#include <stdio.h>

/**
 * @brief Set the floor light object to correct floor
 */
void lights_floor(Elevator *p_elevator);

void ligths_hall(Hall_buttons *p_hall_buttons);

void lights_cab(Cab_buttons *p_cab_buttons);

void lights_door(Elevator *p_elevator);

void lights_stop(Elevator *p_elevator);





