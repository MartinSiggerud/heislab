#pragma once
#include "../driver/elevio.h"
#include "elevator.h"
#include "order.h"
#include "timer.h"
#include <stdio.h>


void movement_open_door(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons);

void movement_close_door(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons);

void movement_obstruction(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons);

void movement_stop(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons);

void movement_elevator(Elevator *p_elevator, Hall_buttons *p_hall_buttons, Cab_buttons *p_cab_buttons);

