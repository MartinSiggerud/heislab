#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include "driver/elevio.h"
#include "modules/startup.h"
#include "modules/elevator.h"
#include "modules/order.h"



int main(){
    printf("=== Elevator Program ===\n");
    printf("press ctrl + c to exit\n");

    Elevator elevator;
    Hall_buttons hall_buttons;
    Cab_buttons cab_buttons;

    int startup_check = startup_initialize(&elevator,&hall_buttons,&cab_buttons);
    while(startup_check){
        int floor = elevio_floorSensor();
        //printf("floor: %d \n",floor);

        //printf("current_floor: %d \n",element.current_floor);
        //printf("next_floor: %d \n",element.next_floor);
        
        order_set_hall_buttons(&hall_buttons, &elevator);
        order_set_cab_buttons(&cab_buttons, &elevator);

        order_find_next_floor(&elevator,&hall_buttons,&cab_buttons);
        order_check_next_floor(&elevator,&hall_buttons,&cab_buttons);

        // testing button
        set_hall_buttons(&hall_buttons);
        int hall_up1 = hall_buttons.hall_up_buttons[1];
        int hall_down1 = hall_buttons.hall_down_buttons[1];
        int hall_up2 = hall_buttons.hall_up_buttons[2];
        int hall_down2 = hall_buttons.hall_down_buttons[2];
        int hall_up0 = hall_buttons.hall_up_buttons[0];
        int hall_down0 = hall_buttons.hall_down_buttons[0];
        printf("hall up: %d \n",hall_up0);
        printf("hall down: %d \n",hall_down0);
        printf("hall up: %d \n",hall_up1);
        printf("hall down: %d \n",hall_down1);
        printf("hall up: %d \n",hall_up2);
        printf("hall down: %d \n",hall_down2);
        /*
        set_cab_buttons(&cab_buttons);
        int cab0 = cab_buttons.button[0];
        int cab1 = cab_buttons.button[1];
        int cab2 = cab_buttons.button[2];
        int cab3 = cab_buttons.button[3];
        printf("cab: %d \n",cab0);
        printf("cab: %d \n",cab1);
        printf("cab: %d \n",cab2);
        printf("cab: %d \n",cab3);
        */

    
        

        //element.next_floor = 3;
        //elevator_movement(&element);
        
        

        // if(floor == 0){
        //    elevio_motorDirection(DIRN_UP);
        // }

        // if(floor == N_FLOORS-1){
        //    elevio_motorDirection(DIRN_DOWN);
        // }


        for(int f = 0; f < N_FLOORS; f++){
            for(int b = 0; b < N_BUTTONS; b++){
                int btnPressed = elevio_callButton(f, b);
                elevio_buttonLamp(f, b, btnPressed);
            }
        }

        set_floor_light();

       

    
        if(elevio_obstruction()){
            elevio_stopLamp(1);
        } else {
            elevio_stopLamp(0);
        }
        
        if(elevio_stopButton()){
            elevio_motorDirection(DIRN_STOP);
            break;
        }
        
        nanosleep(&(struct timespec){0, 20*1000*1000}, NULL);
    }
    return 0;
}
